# Assignment 01

## *Knee deep into phylogenetics*


### Deadline:

May ~~6th~~ 15th 2020


### Delivery format:

* English or Portuguese
* PDF


### Delivery method:

E-mail


### Your task:

For this assignment you will have to:

* Search the bibliography for a research paper that performs phylogenetic analyses;
* Understand the proposed biological problem;
* Obtain the sequences used in that research paper;
* Reproduce (or improve upon) the analyses therein performed (you do not have to use all the methos present in the original paper, only those you deem more important);
* Interpret the obtained phylogenetic trees;
* Compare your results to those of the original paper;
* Make sure your analyses are reproducible;
* Write a report on what you did;


### In detail:

#### Introduction

A short (1-3 pages) section describing:
  * The original biological problem;
  * The used methods;
  * The conclusions from the original paper;

#### Materials & Methods

A section where you **detail** how your analyses were performed:
  * Which methods were used **for each step**;
  * Which software was used;
  * Where can the used scripts be found;

#### Results

Write about what your methods revealed. Report only **observable facts** here.
  * Include any phylogenetic trees you deem necessary;
  * Describe their most important features;
    * Include both tree topology and support;

#### Discussion

Use this section to interpret the results in a biological context.
  * How does the tree help solve the biological issue?
  * How does it compare to the one in the original paper?

Optionally, finish with a *conclusion* section if you think it makes sense in your specific case


### Hints:

* To make your analyses **reproducible**, detail them as much as you can (ideally in scripts), so that you can fully repeat them 5 years later;
* Include any commands and parameters you use;
* Do not hesitate to ask any questions you may have!
