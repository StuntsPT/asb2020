# Assignment 02

## *High Throughput Sequencing at your fingertips*


### Deadline:

June 24th 2020


### Delivery format:

* English or Portuguese
* PDF


### Delivery method:

E-mail


### The data:

For this assignment you will be provided with [a pre-built High Throughput Sequencing (HTS) dataset](https://gitlab.com/StuntsPT/small_test_dataset/-/tree/master/Cornales).
Along with the data you will find a small `.txt` file with important information regarding the data.

### Backstory

* Botanists have found a plant population with several unique phenotypic traits, which they called "GreenLeaf" (other known similar plants has almost black leaves). Some claim this population is a new species, while other claim the differences are irrelevant.
* After sequencing several genes using sanger technology, no meaningfull differentiation between the "BlackLeaf" and "GreenLeaf" groups was found.
* As such, they have now sequenced many populations of "BlackLeaf" individuals and two "GreenLeaf" individuals using RAD-Seq.

### Your Task:

* Figure out if the data has the resolution to distinguish the "GreenLeaf" group from the reminder taxa.
* Are there any other groups that you can identify?
* Write a report on what you did to try to answer that question;
* Keep in mind that your are not analysing a full dataset, but rather a small subset what is considered "normal".


### In detail:

* Write an *introduction* section describing the sequencing method you are working with;
  * Highlight the importance of HTS data on being able to perform the task you are resolving;
* Make sure you have a *Materials & Methods* section where you **detail** how your analyses were performed;
  * Do not forget to describe the dataset as best you can!
* Include a *results* section where you describe the results you have obtained;
* Interpret the results in a biological context in the *Discussion* section;
* Optionally, finish with a *conclusion* section if you think it makes sense in your specific case;


### Hints:

* Make your analyses **reproducible** (detail them as much as you can, so that you can fully repeat them 5 years later);
* Include any commands you use, if you think it is adequate/relevant;
* This dataset might take up to 1h to analyse. Plan in advance;
* DO NOT FORGET TO INCLUDE REFERENCES!!
* Do not hesitate to ask any questions you may have via email or Discord;

### Notes:

* Original data this mockup was derived from can be found in the [dryad repository](https://doi.org/10.5061/dryad.bb3h52t) (It was heavily modified, though!) 
* Original research paper: [Resolving relationships and phylogeographic history of the Nyssa sylvatica complex using data from RAD-seq and species distribution modeling](https://doi.org/10.1016/j.ympev.2018.04.001)
