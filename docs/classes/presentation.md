### classes[0] = "Presentation"

#### Análise de Sequências Biológicas 2019-2020

![Logo EST](presentation_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

## Practical info

<ul>
  <li class="fragment">Schedule: Wednesdays - 9:00 to 13:00, **Room 1.12**</li>
  <li class="fragment">Questions? f.pina.martins@estbarreiro.ips.pt</li>
  <li class="fragment">[Moodle](https://moodle.ips.pt/1920/course/view.php?id=2460)</li>
</ul>

---

## Avaliação

<ul>
  <li class="fragment"><font color="orange">Contínua</font> **OU** <font color="deeppink">exame 1ª e/ou 2ª época</font></li>
  <ul>
    <li class="fragment"><font color="orange">Avaliação contínua:</font></li>
    <ul>
      <li class="fragment"><font color="orange">20% - Contexto sala de aula <font color="red">(3 componentes)</font>:</font/></li>
      <ul>
        <li class="fragment"><font color="red">10% - "Homework"</font></li>
        <li class="fragment"><font color="red">5% - Participação</font></li>
        <li class="fragment"><font color="red">5% - Postura/interesse</font></li>
      </ul>
      <li class="fragment"><font color="orange">30% - 1º trabalho</font/></li>
      <li class="fragment"><font color="orange">30% - 2º trabalho</font/></li>
      <li class="fragment"><font color="orange">20% - Teste</font/></li>
      <li class="fragment"><font color="orange">Sem notas mínimas</font/></li>
    </ul>
  <li class="fragment">Realizar <font color="orange">3/4 da avaliação contínua</font> impede accesso a <font color="deeppink">exame de 1ª época</font/></font></li>
  </ul>
</ul>

---

## What are "biological sequences"?

<ul>
  <li class="fragment">A biological sequence is a single, continuous molecule of nucleic acid or protein.</li>
    <ul>
    <li class="fragment">Can represent multiple types of molecules</li>
    <li class="fragment">Can represent multiple types of data</li>
    </ul>
</ul>

|||

## Such as?

<ul>
  <li class="fragment">By molecule type:</li>
  <ul>
    <li class="fragment"><font color="orange">DNA</font></li>
    <li class="fragment"><font color="green">RNA</font></li>
    <li class="fragment"><font color="cyan">Protein</font></li>
  </ul>
  <li class="fragment">By data structure type:</li>
    <ul>
    <li class="fragment"><font color="purple">Single sequence</font></li>
    <li class="fragment"><font color="yellow">Multiple sequences</font></li>
    <ul>
      <li class="fragment">Can be organized as:</li>
      <ul>
        <li class="fragment"><font color="#FFCC66">Independent sequences</font></li>
        <li class="fragment"><font color="navy">Alignment</font></li>
        <li class="fragment"><font color="grey">Mapping</font></li>
        <li class="fragment"><font color="deeppink">Assembly</font></li>
      </ul>
    </ul>
  </ul>
</ul>

---

## Sooo... Why do we analyse them?

* Phylogenetic trees <!-- .element: class="fragment" data-fragment-index="1" -->
* Population genetics/genomics <!-- .element: class="fragment" data-fragment-index="2" -->
* Organism identification <!-- .element: class="fragment" data-fragment-index="3" -->
* Natural selection detection <!-- .element: class="fragment" data-fragment-index="4" -->
* Finding gene variants <!-- .element: class="fragment" data-fragment-index="5" -->
* Disease prediction <!-- .element: class="fragment" data-fragment-index="6" -->
* Association studies <!-- .element: class="fragment" data-fragment-index="7" -->
* etc.. <!-- .element: class="fragment" data-fragment-index="8" -->

---

## Overview

* Sequence concepts revision <!-- .element: class="fragment" data-fragment-index="1" -->
* Setting up a working environment <!-- .element: class="fragment" data-fragment-index="2" -->
* Sequence formats <!-- .element: class="fragment" data-fragment-index="3" -->
* Getting familiar with Open source software <!-- .element: class="fragment" data-fragment-index="4" -->
* Database access <!-- .element: class="fragment" data-fragment-index="5" -->
* Sequence alignments <!-- .element: class="fragment" data-fragment-index="6" -->
* An intro to Phylogenetics <!-- .element: class="fragment" data-fragment-index="7" -->
* High Throughput Sequencing data analyses <!-- .element: class="fragment" data-fragment-index="8" -->

---

## References

* [NCBI Toolbox](https://www.ncbi.nlm.nih.gov/IEB/ToolBox/SDKDOCS/BIOSEQ.HTML)
